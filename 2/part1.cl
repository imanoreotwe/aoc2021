#! /usr/bin/sbcl --script

(defun sum-expr (lst)
  (apply #'+ lst))

(defun readfile (filename)
  ;; opens and reads file

  (let ((nums '()))
    (with-open-file (stream filename)
      (do 
        ((nextline (read-line stream nil) (read-line stream nil)))
        ((null nextline))
        (setq nums (cons (parse-integer nextline) nums)))
      (reverse nums))))

(defun find-position (moves)
  (let ((pos (list 0 0)))
    (labels ((findr (list)
                    (setq cur (if (= 0 (length nums))
                                (cdr prev)
                                (append (if (< 2 (length prev))
                                          (cdr prev)
                                          prev)
                                        (list (first nums)))))
                    (cond ((= 0 (length cur)) (return-from findr count))
                          ((or (> 3 (length cur))
                               (> 3 (length prev))) ())
                          ((< (sum-expr prev) (sum-expr cur)) (setq count (+ 1 count))))
                      (setq prev cur)
                      (findr (cdr nums))))
      (findr numlist))))

(print (find-positive-deltas (readfile "./input2")))
